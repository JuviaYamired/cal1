/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package calculadora;
import java.util.Scanner;
/**
 *
 * @author Juvia
 */
public class Calculadora {

    public static int suma(int x,int y){
        return x+y;
    }
    
    public static int resta(int x,int y){
        return x-y;
    }
    
    public static int multiplicacion(int x,int y){
        return x*y;
    }
    
    public static int division(int x,int y){
        return x/y;
    }
    
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        int a,b;
        a = s.nextInt();
        b = s.nextInt();
        System.out.println("la suma es " + suma(a,b));
        System.out.println("la resta es " + resta(a,b));
        System.out.println("la multiplicacion es " + multiplicacion(a,b));
        System.out.println("la division es " + division(a,b));
    }
    
}
